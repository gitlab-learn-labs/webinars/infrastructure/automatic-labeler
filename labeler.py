import requests
from datetime import datetime, timedelta
import sys

AT = sys.argv[1]


# Test for sample project testing https://gitlab.com/gitlab-learn-labs/sample-project-testing
url     = 'https://gitlab.com/api/v4/groups/58416235/projects/?include_subgroups=true&per_page=500'
headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
res = requests.get(url, headers=headers)
res_json = res.json()

today = datetime.today() 

# today_formatted = datetime.today().strftime('%Y-%m-%d')
two_week = timedelta(days=14)

two_weeks_out = today + two_week
two_weeks_out = two_weeks_out.strftime('%Y-%m-%d')

# Loop through all of the projects in CS shared demo space and its subgroups
for x in res_json:

    #TODO
    # Auto assign if me, ignore the test account
    # add logic to not add the issue if one already exists 


    # Fetch the last commit author & commit info
    project_id = x['id']
    print('project id ' + str(project_id))
    url_commit = 'https://gitlab.com/api/v4/projects/' + str(project_id) + '/repository/commits'
    headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
    res_commit = requests.get(url_commit, headers=headers)
    res_json_commit = res_commit.json()
    if res_json_commit != []:
        # Create an issue for the author
        commit_author = res_json_commit[0]['author_name']
        print('Author: ' + res_json_commit[0]['author_name'])
        commit_date = res_json_commit[0]['committed_date']
        created_check = datetime.strptime(x['created_at'], "%Y-%m-%dT%H:%M:%S.%fZ")
        updated_check = datetime.strptime((datetime.strptime(str(datetime.fromisoformat(commit_date)), "%Y-%m-%d %H:%M:%S%z")).strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S")
        today_second_format = datetime.strptime(today.strftime("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S")

        created_boolean = (today - created_check).days < 14
        updated_boolean =(today_second_format - updated_check).days < 14
        stale_boolean =(today_second_format - updated_check).days > 31
        year_stale_boolean =(today_second_format - updated_check).days > 365
        
        # checking if project has been updated in a month 
        if stale_boolean:
            # check if issue already has name
            url_issue_check = 'https://gitlab.com/api/v4/projects/' + str(x['id']) + '/issues?labels=reminder&state=opened'
            headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
            res_check = requests.get(url_issue_check, headers=headers)
            res_json_check = res_check.json()
            # 'author_name': 'Lars Blockken', 'author_email': 'lblockken@gitlab.com', 'authored_date': '2023-06-06T08:29:12.000+00:00', 'committer_name': 'Lars Blockken', 'committer_email': 'lblockken@gitlab.com', 'committed_date': '2023-06-06T08:29:12.000+00:00'
            print('res_json_check value: ' + str(res_json_check))
            
            if res_json_check == []:
                # Get the gitlabid from the author name
                url_id = 'https://gitlab.com/api/v4/users?search=' + commit_author
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                res_id = requests.get(url_id, headers=headers)
                res_json_id = res_id.json()
                gitlab_id = 10994493

                # Get provisioned usersGET /projects/:id/users
                url_users = 'https://gitlab.com/api/v4/projects/' + str(project_id) + '/users?search=' + str(commit_author)
                print(url_users)
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                res_users = requests.get(url_users, headers=headers)
                res_json_users = res_users.json()
                user_in_project = False
                # 'author_name': 'Lars Blockken', 'author_email': 'lblockken@gitlab.com', 'authored_date': '2023-06-06T08:29:12.000+00:00', 'committer_name': 'Lars Blockken', 'committer_email': 'lblockken@gitlab.com', 'committed_date': '2023-06-06T08:29:12.000+00:00'
                matching_name_ids = []
                for u in res_json_id:
                    if u['name'] == commit_author:
                        # gitlab_id = u['id']
                        matching_name_ids.append(u['id'])

                for z in res_json_users:
                    for y in matching_name_ids:
                        if z['id'] == y:
                            user_in_project = True
                            gitlab_id = y

                url     = 'https://gitlab.com/api/v4/projects/' + str(project_id) + '/issues'
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                # Add people to ignore list
                if gitlab_id == 11739260 or user_in_project == False or gitlab_id == 3222377:
                    payload_issue = '''
                { "title": "Automated Update Reminder",
                "description": "This is an automated issue to remind the last committer to continue updating this project so that it stays relevant for others to use. If you are the most recent updater but not who should be updating this issue, please reassign it. This issue will stick until a fix is made and the issue is closed", 
                "assignee_ids": [10994493],
                "labels": "reminder",
                "due_date": "''' + two_weeks_out + '''"  }
                '''
                else:
                    payload_issue = '''
                { "title": "Automated Update Reminder",
                "description": "This is an automated issue to remind the last committer to continue updating this project so that it stays relevant for others to use. If you are the most recent updater but not who should be updating this issue, please reassign it. This issue will stick until a fix is made and the issue is closed", 
                "assignee_ids": [''' + str(gitlab_id) +  ''', 10994493],
                "labels": "reminder",
                "due_date": "''' + two_weeks_out + '''"  }
                '''
                res_issue = requests.post(url, payload_issue, headers=headers)
                res_json_issue = res_issue.json()

        # logan here

        # Check if the compliance framework variable is empty so we dont overwrite ones used in demos
        
        # if not x['compliance_frameworks']:
        if (x.get('compliance_frameworks') == None):
            if created_boolean :
                url     = 'https://gitlab.com/api/graphql'
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                
                payload = '''
        mutation {
        projectSetComplianceFramework(
            input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3498"}
        ) {
            project {
            complianceFrameworks {
                nodes {
                name
                }
            }
            }
        }
        }
                '''

                res = requests.post(url, json={"query": payload}, headers=headers)
            #  Created takes presidence, so only add updated if created does not apply
            elif updated_boolean and (created_boolean is False) :

                url     = 'https://gitlab.com/api/graphql'
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                
                payload = '''
        mutation {
        projectSetComplianceFramework(
            input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3497"}
        ) {
            project {
            complianceFrameworks {
                nodes {
                name
                }
            }
            }
        }
        }
                '''
                res = requests.post(url, json={"query": payload}, headers=headers)
            # need to check if there hasnt been an update for a year
            elif year_stale_boolean:
                url     = 'https://gitlab.com/api/graphql'
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                
                payload = '''
        mutation {
        projectSetComplianceFramework(
            input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3881"}
        ) {
            project {
            complianceFrameworks {
                nodes {
                name
                }
            }
            }
        }
        }
                '''
                res = requests.post(url, json={"query": payload}, headers=headers)
            else:
                print('Label was empty and should be empty')
        else:
            auto_label_check = x['compliance_frameworks'] != ['Recently Added Project'] and x['compliance_frameworks'] != ['Recently Updated Project'] and x['compliance_frameworks'] != ['Stale Project']
            # If a framework was applied, we need to make sure we dont overwrite it (unless its added or updated labels)
            if auto_label_check:
                print('Kepping existing label (wasnt updated or added or stale)')
            # Check if correct label is still apllied
            elif updated_boolean and  x['compliance_frameworks'] == ['Recently Updated Project']:
                print('still needs updated label')
            #Check if wrong label was applied or it used to be recently added but now is recently updated
            elif updated_boolean and  x['compliance_frameworks'] != ['Recently Updated Project'] and auto_label_check == False:
                url     = 'https://gitlab.com/api/graphql'
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                
                payload = '''
        mutation {
        projectSetComplianceFramework(
            input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3497"}
        ) {
            project {
            complianceFrameworks {
                nodes {
                name
                }
            }
            }
        }
        }
                '''
                # res = requests.post(url, data=payload, headers=headers)
                res = requests.post(url, json={"query": payload}, headers=headers)
            #  Two next statements are more of a proof check that it still should be recently updated or it wasnt mislabeled by a user
            elif created_boolean and x['compliance_frameworks'] == ['Recently Added Project']:
                print('still needs created label')
            elif created_boolean and  x['compliance_frameworks'] != ['Recently Added Project'] and auto_label_check == False:
                url     = 'https://gitlab.com/api/graphql'
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                
                payload = '''
        mutation {
        projectSetComplianceFramework(
            input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: "gid://gitlab/ComplianceManagement::Framework/3498"}
        ) {
            project {
            complianceFrameworks {
                nodes {
                name
                }
            }
            }
        }
        }
                '''
                # res = requests.post(url, data=payload, headers=headers)
                res = requests.post(url, json={"query": payload}, headers=headers)
            #  This should only remove the updated or added labels when they are no longer needed
            else:
                url     = 'https://gitlab.com/api/graphql'
                headers = {"Content-Type" : "application/json", 'PRIVATE-TOKEN' : f"{AT}"}
                
                payload = '''
        mutation {
        projectSetComplianceFramework(
            input: {projectId: "gid://gitlab/Project/''' + str(x['id']) + '''", complianceFrameworkId: null}
        ) {
            project {
            complianceFrameworks {
                nodes {
                name
                }
            }
            }
        }
        }
                '''
                # res = requests.post(url, data=payload, headers=headers)
                res = requests.post(url, json={"query": payload}, headers=headers)
        # ends the if clause
